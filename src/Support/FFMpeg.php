<?php

namespace SamSidney\LaravelFFMpeg\Support;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \SamSidney\LaravelFFMpeg\Http\DynamicHLSPlaylist dynamicHLSPlaylist($disk)
 * @method static \SamSidney\LaravelFFMpeg\MediaOpener fromDisk($disk)
 * @method static \SamSidney\LaravelFFMpeg\MediaOpener fromFilesystem(\Illuminate\Contracts\Filesystem\Filesystem $filesystem)
 * @method static \SamSidney\LaravelFFMpeg\MediaOpener open($path)
 * @method static \SamSidney\LaravelFFMpeg\MediaOpener openUrl($path, array $headers)
 * @method static \SamSidney\LaravelFFMpeg\MediaOpener cleanupTemporaryFiles()
 *
 * @see \SamSidney\LaravelFFMpeg\MediaOpener
 */
class FFMpeg extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'laravel-ffmpeg';
    }
}
