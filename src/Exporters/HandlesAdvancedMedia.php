<?php

namespace SamSidney\LaravelFFMpeg\Exporters;

use FFMpeg\Format\FormatInterface;
use SamSidney\LaravelFFMpeg\FFMpeg\AdvancedOutputMapping;
use SamSidney\LaravelFFMpeg\Filesystem\Media;

trait HandlesAdvancedMedia
{
    /**
     * @var \Illuminate\Support\Collection
     */
    protected $maps;

    public function addFormatOutputMapping(FormatInterface $format, Media $output, array $outs, $forceDisableAudio = false, $forceDisableVideo = false)
    {
        $this->maps->push(
            new AdvancedOutputMapping($outs, $format, $output, $forceDisableAudio, $forceDisableVideo)
        );

        return $this;
    }
}
