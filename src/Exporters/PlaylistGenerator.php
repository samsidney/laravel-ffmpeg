<?php

namespace SamSidney\LaravelFFMpeg\Exporters;

use SamSidney\LaravelFFMpeg\Drivers\PHPFFMpeg;

interface PlaylistGenerator
{
    public function get(array $playlistMedia, PHPFFMpeg $driver): string;
}
